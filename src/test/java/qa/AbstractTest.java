package qa;

import static common.CheckUtils.waitForDashboardPageLoaded;
import static common.CheckUtils.waitForFragmentVisible;

import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.testng.Arquillian;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import fragments.DashboardPage;
import fragments.LoginFragment;

public abstract class AbstractTest extends Arquillian {

    @Drone
    protected WebDriver browser;

    @FindBy(id = "form-login")
    protected LoginFragment loginFragment;

    @FindBy(id = "page")
    protected DashboardPage dashboardPage;

    protected static final String LOGIN_PAGE = "login.jsp";

    protected static final String COMMON_PHRASE = "XenKute";

    public void openUrl(String url) {
        browser.get(getRootUrl() + url);
    }

    public void openDashboardPage() {
        openUrl("secure/Dashboard.jspa");
        waitForDashboardPageLoaded();
    }

    public String getRootUrl() {
        return "https://jira.atlassian.com/";
    }

    public void signIn(String username, String password) {
        openUrl(LOGIN_PAGE);
        waitForFragmentVisible(loginFragment);
        loginFragment.signIn(username, password);
    }
}

package qa;

import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;

@Test(groups = {"search-issues"}, description = "Existing issues can be found via JIRA's search")
public class SearchIssuesTest extends AbstractTest {

    @Test
    public void init() {
        signIn("vhtien1986@gmail.com", "atlassian");
        openDashboardPage();
    }

    @Test(dependsOnMethods = { "init" })
    public void searchIssues() {
        List<WebElement> issues = dashboardPage.searchIssues(COMMON_PHRASE);
        assertTrue(
            Iterables.all(
                Collections2.transform(issues, new Function<WebElement, String>() {
                    @Override
                    public String apply(WebElement e) {
                        return e.getAttribute("title");
                    }
                }),
                new Predicate<String>() {
                    @Override
                    public boolean apply(String element) {
                        return element.contains(COMMON_PHRASE);
                    }
                }
            )
        );
    }
}

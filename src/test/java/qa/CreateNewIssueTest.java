package qa;

import org.testng.annotations.Test;

import entity.Issue;

public class CreateNewIssueTest extends AbstractTest {

    @Test
    public void init() {
        signIn("vhtien1986@gmail.com", "atlassian");
        openDashboardPage();
    }

    @Test(dependsOnMethods = {"init"})
    public void createNewIssues() {
        dashboardPage.createIssues(new Issue(COMMON_PHRASE + " " + System.currentTimeMillis()));
    }
}

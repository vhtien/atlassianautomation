package entity;

public class Issue {

    private String summary;

    public Issue(String summary) {
        this.summary = summary;
    }

    public String getSummary() {
        return summary;
    }
}

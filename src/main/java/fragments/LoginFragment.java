package fragments;

import static common.CheckUtils.waitForElementVisible;
import static common.CheckUtils.waitForFragmentNotVisible;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginFragment extends AbstractFragment {

    @FindBy
    private WebElement username;

    @FindBy
    private WebElement password;

    @FindBy(id = "login-submit")
    private WebElement submitButton;

    public void signIn(String username, String password) {
        waitForElementVisible(this.username).clear();
        this.username.sendKeys(username);
        waitForElementVisible(this.password).clear();
        this.password.sendKeys(password);
        waitForElementVisible(submitButton).click();
        waitForFragmentNotVisible(this);
    }
}

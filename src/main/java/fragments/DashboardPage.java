package fragments;

import static common.CheckUtils.waitForCollectionIsNotEmpty;
import static common.CheckUtils.waitForElementVisible;

import java.util.List;

import org.jboss.arquillian.graphene.Graphene;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.google.common.base.Predicate;

import entity.Issue;

public class DashboardPage extends AbstractFragment {

    @FindBy(id = "create_link")
    private WebElement createIssuesButton;

    @FindBy
    private WebElement quickSearchInput;

    @FindBy(css = ".issue-list li")
    private List<WebElement> issues;

    @FindBy(css = ".toolbar-item #edit-issue")
    private WebElement editIssueButton;

    @FindBy(id = "summary-val")
    private WebElement summaryValue;

    private static final By BY_CREATE_ISSUE_DIALOG = By.id("create-issue-dialog");

    private static final By BY_EDIT_ISSUE_DIALOG = By.id("edit-issue-dialog");

    public DashboardPage createIssues(Issue issue) {
        waitForElementVisible(createIssuesButton).click();

        Graphene.createPageFragment(IssueDialog.class,
                waitForElementVisible(BY_CREATE_ISSUE_DIALOG, browser))
                .createIssue(issue);

        return this;
    }

    public List<WebElement> searchIssues(String pattern) {
        waitForElementVisible(quickSearchInput).click();
        quickSearchInput.sendKeys(pattern);
        quickSearchInput.sendKeys(Keys.ENTER);

        waitForCollectionIsNotEmpty(issues);
        return issues;
    }

    public DashboardPage updateIssue(final Issue issue) {
        waitForElementVisible(editIssueButton).click();

        Graphene.createPageFragment(IssueDialog.class,
                waitForElementVisible(BY_EDIT_ISSUE_DIALOG, browser))
                .updateIssue(issue);

        Graphene.waitGui().until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver browser) {
                return issue.getSummary().equals(waitForElementVisible(summaryValue).getText().trim());
            }
        });

        return this;
    }
}

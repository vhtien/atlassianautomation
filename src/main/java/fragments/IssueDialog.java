package fragments;

import static common.CheckUtils.waitForElementNotPresent;
import static common.CheckUtils.waitForElementVisible;
import static common.CheckUtils.waitForFragmentVisible;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import entity.Issue;

public class IssueDialog extends AbstractFragment {

    @FindBy
    private WebElement summary;

    @FindBy(id = "create-issue-submit")
    private WebElement createButton;

    @FindBy(id = "edit-issue-submit")
    private WebElement editButton;

    private static final By BY_MESSAGE = By.cssSelector(".aui-message.success.closeable");

    public void createIssue(Issue issue) {
        waitForFragmentVisible(this);
        waitForElementVisible(summary).clear();
        summary.sendKeys(issue.getSummary());
        waitForElementVisible(createButton).click();
        waitForElementNotPresent(this.getRoot());
        assertTrue(waitForElementVisible(BY_MESSAGE, browser)
                .getText()
                .matches("Issue TST-\\d+ - XenKute \\d+ has been successfully created."));
    }

    public void updateIssue(Issue issue) {
        waitForFragmentVisible(this);
        waitForElementVisible(summary).clear();
        summary.sendKeys(issue.getSummary());
        waitForElementVisible(editButton).click();
        waitForElementNotPresent(this.getRoot());
    }
}

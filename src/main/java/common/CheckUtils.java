package common;

import java.util.Collection;

import org.jboss.arquillian.graphene.Graphene;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.google.common.base.Predicate;

import fragments.AbstractFragment;

public class CheckUtils {

    private CheckUtils() {
    }

    public static WebElement waitForElementVisible(WebElement element) {
        Graphene.waitGui().until().element(element).is().visible();
        return element;
    }

    public static WebElement waitForElementVisible(By byElement, WebDriver browser) {
        Graphene.waitGui().until().element(byElement).is().visible();
        return browser.findElement(byElement);
    }

    public static void waitForElementNotPresent(WebElement element) {
        Graphene.waitGui().until().element(element).is().not().present();
    }

    public static void waitForFragmentVisible(AbstractFragment fragment) {
        Graphene.waitGui().until().element(fragment.getRoot()).is().visible();
    }

    public static void waitForFragmentNotVisible(AbstractFragment fragment) {
        Graphene.waitGui().until().element(fragment.getRoot()).is().not().visible();
    }

    public static void waitForDashboardPageLoaded() {
        Graphene.waitGui().until().element(By.id("dashboard")).is().visible();
    }

    public static void waitForCollectionIsNotEmpty(final Collection<WebElement> elements) {
        Graphene.waitGui().until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver browser) {
                return !elements.isEmpty();
            }
        });
    }
}

Atlassian automation exercise
====================
Clone this repository and using Eclipse to import this project as Maven project.
Build this project to make sure all libraries have downloaded.
Using TestNG plug-in in Eclipse to run 3 files in src/test/java/qa:
 - CreateNewIssueTest
 - SearchIssueTest
 - UpdateIssueTest 